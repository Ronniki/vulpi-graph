function Graph(options) {
  this.colorLine = options.colorLine || 'rgba(0, 155, 0, 1)'
  this.data = options.data || []
  this.textX = options.textX || ''
  this.textY = options.textY || ''
  this.paddingYTop = 150
  this.paddingYBottom = 150
  this.paddingY = this.paddingYTop + this.paddingYBottom
  this.paddingXLeft = 200
  this.paddingXRight = 100
  this.paddingX = this.paddingXLeft + this.paddingXRight
  this.invert = false
  this.fontSizeBig = '5rem'
  this.fontSizeSmall = '3.8rem'
  this.id = options.id
  this.wrapper = document.querySelector(`#${options.id}`)
  
  this.init = function() {
    const wrapper = document.querySelector(`#${this.id}`)
    const graph = document.createElement('div')
    
    try {
      wrapper.append(graph)
    } catch (e) {
      console.log('Не найден id')
    }
    graph.classList.add('vul-graph')
    graph.append(this.createCanvas())
    graph.style.position = 'relative'

    this.createGrid()
    this.createCheckbox()
    this.createLegendX(this.invert)
    this.createLegendY(this.invert)
    this.createGraphLine(this.invert)
    this.toggleInvert()
    this.watcherForLine()
  }

  this.createCanvas = function () {
    this.canvas = document.createElement('canvas')
    this.canvas.classList.add('vulpi-graph')
    this.canvas.width = this.clientWidth
    this.canvas.height = this.clientHeight
    this.canvas.width = 2000
    this.canvas.height = 2000
    const ctx = this.canvas.getContext('2d')
    ctx.translate(0.5, 0.5);
    return this.canvas
  }

  this.createLegendX = function (invert = false) {
    const ctx = this.canvas.getContext('2d')
    
    const totalStepX = this.getCorrectedAxisXLegendValue(this.data)
    const stepX = (this.canvas.width - this.paddingX) / (totalStepX.countStep)
    
    ctx.fillStyle = 'black'
    ctx.strokeStyle = 'black'
    ctx.lineWidth = 5
    // Text X 
    ctx.font = `${this.fontSizeSmall} Verdana`
    ctx.textAlign = 'left'

    for (let i = 0; i < totalStepX.points.length; i++) {
      if (invert) {
        ctx.fillText(totalStepX.points[i], this.canvas.width - this.paddingXRight - stepX * i, this.canvas.width - 80)
      } else {
        ctx.fillText(totalStepX.points[i], this.paddingXLeft + stepX * i, this.canvas.width - 80)
      }
      // Отсечки X
      ctx.beginPath()
      ctx.moveTo(this.paddingXLeft + stepX * i, this.canvas.height - this.paddingYBottom)
      ctx.lineTo(this.paddingXLeft + stepX * i, this.canvas.height - this.paddingYBottom + 15)
      ctx.stroke()
      
    }
    // Горизонтальная линия
    ctx.beginPath()
    ctx.moveTo(this.canvas.width - this.paddingXRight, this.canvas.height - this.paddingYBottom)
    ctx.lineTo(this.paddingXLeft, this.canvas.height - this.paddingYBottom)
    ctx.stroke()

    ctx.font =  `${this.fontSizeBig} Verdana`
    ctx.textAlign = "left"
    ctx.fillText(this.textX, this.paddingXLeft, this.canvas.height - 5)
  }

  this.createLegendY = function (invert = false) {
    const ctx = this.canvas.getContext('2d')
    const points = this.data

    ctx.fillStyle = 'black'
    ctx.strokeStyle = 'black'
    ctx.lineWidth = 5

    // Text
    ctx.textAlign = 'right'
    const totalStepY = this.getCorrectedAxisYLegendValue(points)
    const stepY = (this.canvas.height - this.paddingY) / (totalStepY.countStep)
    
    ctx.font = `${this.fontSizeSmall} Verdana`
    
    for(let i = 0; i < totalStepY.points.length; i++) {
      // Подпись Y
      ctx.fillText(totalStepY.points[i], this.paddingXLeft - 25, this.canvas.height - this.paddingYBottom - i * stepY)
      // Отсечки Y
      ctx.beginPath()
      ctx.moveTo(this.paddingXLeft - 10,  this.canvas.height - this.paddingYBottom - i * stepY)
      ctx.lineTo(this.paddingXLeft,  this.canvas.height - this.paddingYBottom - i * stepY)
      ctx.stroke()
    }

    ctx.font = `${this.fontSizeBig} Verdana`
    let strY = this.textY
    ctx.textAlign = 'center'
    strY = strY.split('').reverse().join('')
    for (let i = strY.length - 1; i >= 0; i--) {
      ctx.fillText(strY[i], 30, this.canvas.height - (this.paddingYBottom) - i * 50)
    }
    // Line
    ctx.moveTo(this.paddingXLeft, this.paddingYBottom);
    ctx.lineTo(this.paddingXLeft, this.canvas.height - this.paddingYBottom);
    ctx.closePath()
    ctx.stroke();
  }

  this.createGrid = function () {
    const ctx = this.canvas.getContext('2d')
    const points = this.data
    const totalStepY = this.getCorrectedAxisYLegendValue(points)
    const totalStepX = this.getCorrectedAxisXLegendValue(this.data)
    const stepY = (this.canvas.height - this.paddingY) / (totalStepY.countStep)
    const stepX = (this.canvas.width - this.paddingX) / (totalStepX.countStep)

    ctx.strokeStyle = 'rgba(150, 150, 150, 1)'
    ctx.lineWidth = 1

    for(let i = 0; i < totalStepY.points.length; i++) {
      // Отсечки Y
      ctx.beginPath()
      ctx.moveTo(this.canvas.width - this.paddingXRight,  this.canvas.height - this.paddingYBottom - i * stepY)
      ctx.lineTo(this.paddingXLeft,  this.canvas.height - this.paddingYBottom - i * stepY)
      ctx.stroke()
    }
    for (let i = 0; i < totalStepX.points.length; i++) {
      // Отсечки X
      ctx.beginPath()
      ctx.moveTo(this.paddingXLeft + stepX * i, this.canvas.height - this.paddingYBottom)
      ctx.lineTo(this.paddingXLeft + stepX * i, this.paddingYBottom)
      ctx.stroke()
    }
  }

  this.createGraphLine = function (invert = false) {
    const ctx = this.canvas.getContext('2d')
    const points = this.data
    const start = this.calibratePoint(points[0], 0, invert)

    this.lines = []
    ctx.lineWidth = 8
    ctx.beginPath()
    ctx.moveTo(start.x, start.y)
    ctx.strokeStyle = this.colorLine
    ctx.fillStyle = this.colorLine
    if (!points.length) {
      ctx.fillText('Нет данных', this.canvas.width / 2, this.canvas.height / 2)
    }

    for (let i = 0; i < points.length; i++) {
      ctx.lineJoin = "round"
      ctx.lineTo(this.calibratePoint(points[i], i, invert).x, this.calibratePoint(points[i], i, invert).y) 
      this.lines.push({x: this.calibratePoint(points[i], i, invert).x, y: this.calibratePoint(points[i], i, invert).y})
      this.lines.push({x: this.getCenterLine(this.calibratePoint(points[i], i, invert).x, this.calibratePoint(points[i + 1], i + 1, invert).x), y: this.getCenterLine(this.calibratePoint(points[i], i, invert).y, this.calibratePoint(points[i + 1], i + 1, invert).y)})
    }
    ctx.stroke()
  }
  this.getCenterLine = function (currentCoord, nextCoord) {
    if (!nextCoord) return
    if (nextCoord > currentCoord) {
      return (nextCoord - currentCoord) / 2 + currentCoord
    } else if (nextCoord < currentCoord) {
      return (currentCoord - nextCoord) / 2 + nextCoord
    }
    return currentCoord
  }
  this.getCorrectStepX = function (maxPoint, step) {
    if (maxPoint > 0 && maxPoint <= 20) {
      return step
    }
    if (maxPoint > 20 && maxPoint <= 50) {
      return step * 5
    }
    if (maxPoint > 50 && maxPoint <= 100) {
      return step * 10
    }
    return step
  }

  this.getCorrectedAxisYLegendValue = function (arr) {
    const minPoint = this.getMinPoint(arr)
    const maxPoint = this.getMaxPoint(arr)
    const diffBetweenMaxMin = maxPoint - minPoint
    const res = {
      points: [],
      multi: 1
    }

    if (diffBetweenMaxMin > 0 && diffBetweenMaxMin <= 20) {
      res.maxPoint = maxPoint
      res.minPoint = minPoint
      res.multi = 1

      let count = res.minPoint
      while (count <= res.maxPoint) {
        res.points.push(count)
        count++
      }
    } else if (diffBetweenMaxMin > 20 && diffBetweenMaxMin <= 100) {
      res.maxPoint = Math.floor(maxPoint / 10) * 10 + ((maxPoint % 5 != 0) ? 5 : 0)
      res.minPoint = Math.floor(minPoint / 10) * 10
      res.multi = 5

      let count = res.minPoint
      while (count <= res.maxPoint) {
        res.points.push(count)
        count += 5
      }

    } else if (diffBetweenMaxMin > 100 && diffBetweenMaxMin <= 200) {
      res.maxPoint = Math.floor(maxPoint / 10) * 10 + ((maxPoint % 10 != 0) ? 10 : 0)
      res.minPoint = Math.floor(minPoint / 10) * 10
      res.multi = 10

      let count = res.minPoint
      while (count <= res.maxPoint) {
        res.points.push(count)
        count += 10
      }

    } else if (diffBetweenMaxMin > 200 && diffBetweenMaxMin <= 2000) {
      res.maxPoint = Math.floor(maxPoint / 100) * 100 + ((maxPoint % 100 != 0) ? 100 : 0)
      res.minPoint = Math.floor(minPoint / 100) * 100
      res.multi = 100

      let count = res.minPoint
      while (count <= res.maxPoint) {
        res.points.push(count)
        count += 100
      }
    }
    res.totalStep = res.maxPoint - res.minPoint
    res.countStep = res.points.length - 1

    return res
  }

  this.getCorrectedAxisXLegendValue = function (arr) {
    const maxPoint = arr.length
    const res = {
      points: [],
      multi: 1
    }

    if (maxPoint > 0 && maxPoint <= 20) {
      res.maxPoint = maxPoint
      res.multi = 1

      let count = 0
      while (count < maxPoint) {
        res.points.push(count)
        count++
      }
    } else if (maxPoint > 20 && maxPoint <= 100) {
      res.maxPoint = Math.floor(maxPoint / 10) * 10 + ((maxPoint % 5 != 0) ? 5 : 0)
      res.multi = 5

      let count = 0
      while (count < maxPoint) {
        res.points.push(count)
        count += 5
      }
    } else if (maxPoint > 100 && maxPoint <= 200) {
      res.maxPoint = Math.floor(maxPoint / 10) * 10 + ((maxPoint % 10 != 0) ? 10 : 0)
      res.multi = 10

      let count = 0
      while (count < maxPoint) {
        res.points.push(count)
        count += 10
      }
    } else if (maxPoint > 200 && maxPoint <= 2000) {
      res.maxPoint = Math.floor(maxPoint / 100) * 100 + ((maxPoint % 100 != 0) ? 100 : 0)
      res.multi = 100

      let count = 0
      while (count < maxPoint) {
        res.points.push(count)
        count += 100
      }
    } else if (maxPoint > 2000 && maxPoint <= 10000) {
      res.maxPoint = Math.floor(maxPoint / 100) * 100 + ((maxPoint % 500 != 0) ? 500 : 0)
      res.multi = 500
      
      let count = 0
      while (count < maxPoint) {
        res.points.push(count)
        count += 500
      }
    }
    res.countStep = res.points.length - 1
    return res
  }

  this.calcHeightAndWidthRect = function (point) {
    const points = this.data
    const maxPoint = this.getMaxPoint(points)
    const stepX = (this.canvas.width - this.paddingX) / (points.length)
    const stepY = (this.canvas.height - this.paddingY) / maxPoint
    const result = {}

    result.width = stepX
    result.height = stepY * point

    return result
  }

  this.calibratePoint = function (point, index = 0, invert = false) {
    const result = {}
    const points = this.data
    const totalStepX = this.getCorrectedAxisXLegendValue(points)
    const maxPointX = totalStepX.maxPoint
    const totalStepY = this.getCorrectedAxisYLegendValue(points)
    const stepx = (this.canvas.width - this.paddingX) / (maxPointX)
    const stepX = this.getCorrectStepX(maxPointX, stepx)
    const stepY = (this.canvas.height - this.paddingY) / (totalStepY.totalStep)
    let multiX = totalStepX.multi

    // Перевод данных массива в координаты полотна canvas
    if (invert) {
      result.x = this.canvas.width - this.paddingXRight - index * stepX / multiX
      result.y = this.canvas.height - this.paddingYBottom - (point - totalStepY.minPoint) * stepY
    } else {
      result.x = this.paddingXLeft + index * stepX / multiX
      result.y = this.canvas.height - this.paddingYBottom - (point - totalStepY.minPoint) * stepY
    }
    return result
  }

  this.createNotice = function (line, index) {
    const ctx = this.canvas.getContext('2d')
    const points = this.data
    ctx.strokeStyle = 'rgba(0,0,0, 0)'
    ctx.beginPath()
    ctx.font = this.fontSizeBig + ' Verdana'
    ctx.fillStyle = 'rgba(0,0,0, 1)'
    ctx.shadowColor='rgba(250, 0, 0, 1)'
    ctx.shadowBlur=5
    ctx.lineWidth=5
    ctx.fillText(points[index], line.x, line.y - 40)
    ctx.fillStyle = 'rgba(0,0,0, 1)'
    ctx.shadowColor='rgba(0, 0, 0, 1)'
    ctx.shadowBlur=5
    ctx.lineWidth=5
    ctx.fillText(points[index], line.x, line.y - 40)
    ctx.shadowBlur=0
    ctx.fillStyle="rgba(250, 250, 250, 1)"
    ctx.fillText(points[index], line.x, line.y - 40)

    ctx.stroke()
    ctx.fill()

  }

  this.watcherForLine = function () {
    const width = this.canvas.width
    const height = this.canvas.height
    const ctx = this.canvas.getContext('2d')

    this.canvas.addEventListener('mousemove', (e) => {
      const target = this.wrapper.getBoundingClientRect()
      const x = (e.clientX - target.left) * (width / target.width)
      const y = (e.clientY - target.top) * (height / target.height)
      let count = 0
      
      for (let i = 0; i < this.lines.length; i++) {
        if (i % 2 != 0) count++
        ctx.beginPath()
        ctx.moveTo(this.lines[i].x, this.lines[i].y)

        // Линия толще для более удобного срабатывания наведения мыши
        ctx.lineWidth = 30
        // Корректировка i для последнего элемента в массиве
        ctx.lineTo(this.lines[(i == this.lines.length - 1) ? this.lines.length - 1 : i + 1].x, this.lines[(i == this.lines.length - 1) ? this.lines.length - 1 : i + 1].y)
        const check = ctx.isPointInStroke(x,y)

        if (check) {
          ctx.clearRect(0, 0, width, height)
          
          this.createGrid()
          this.createGraphLine(this.invert)
          this.createLegendX(this.invert)
          this.createLegendY()
          
          // Отрисовка точки при наведении курсора на линию
          ctx.fillStyle = this.colorLine
          if (i % 2 != 0) {
            ctx.beginPath()
            ctx.arc(this.lines[(i == this.lines.length - 1) ? this.lines.length - 1 : i + 1].x, this.lines[(i == this.lines.length - 1) ? this.lines.length - 1 : i + 1].y, 16, 0,2*Math.PI)
            ctx.arc(this.lines[(i == this.lines.length - 1) ? this.lines.length - 1 : i + 1].x, this.lines[(i == this.lines.length - 1) ? this.lines.length - 1 : i + 1].y, 16, 0,2*Math.PI)
            ctx.fill()
            ctx.moveTo(this.lines[(i == this.lines.length - 1) ? this.lines.length - 1 : i + 1].x, this.lines[(i == this.lines.length - 1) ? this.lines.length - 1 : i + 1].y)
            ctx.stroke()
            this.createNotice(this.lines[(i == this.lines.length - 1) ? this.lines.length - 1 : i + 1], count)
          } else {
            ctx.beginPath()
            ctx.arc(this.lines[(i == this.lines.length - 1) ? this.lines.length - 1 : i].x, this.lines[(i == this.lines.length - 1) ? this.lines.length - 1 : i].y, 16, 0,2*Math.PI)
            ctx.arc(this.lines[(i == this.lines.length - 1) ? this.lines.length - 1 : i].x, this.lines[(i == this.lines.length - 1) ? this.lines.length - 1 : i].y, 16, 0,2*Math.PI)
            ctx.fill()
            ctx.moveTo(this.lines[(i == this.lines.length - 1) ? this.lines.length - 1 : i].x, this.lines[(i == this.lines.length - 1) ? this.lines.length - 1 : i].y)
            ctx.stroke()
            this.createNotice(this.lines[(i == this.lines.length - 1) ? this.lines.length - 1 : i], count)
          }
        }
      }
    })
  }

  this.getMinPoint = function (arr) {
    return Math.min.apply(null, arr)
  }

  this.getMaxPoint = function (arr) {
    return Math.max.apply(null, arr)
  }

  this.createCheckbox = function () {
    const label = document.createElement('label')
    const checkbox = document.createElement('input')
    try {
      const graph = this.wrapper.querySelector('.vul-graph')
      checkbox.setAttribute('type', 'checkbox')
      checkbox.setAttribute('value', 'Invert Y')
      label.setAttribute('class', 'vul-graph__label')
      
      checkbox.innerHTML = 'Invert Y'
      label.append(checkbox)
      label.append('Invert Y')
      graph.append(label)
    } catch (e) {
      console.log('Не найден id')
    }
  }

  this.toggleInvert = function () {
    try {
      const checkbox = this.wrapper.querySelector('input[type="checkbox"]')
      
      checkbox.addEventListener('click', (e) => {
        this.invert = e.target.checked
        const ctx = this.canvas.getContext('2d')
        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
        this.createGrid(this.invert)
        this.createGraphLine(this.invert)
        this.createLegendY(this.invert)
        this.createLegendX(this.invert)
      })
    } catch (e) {
      console.log('Не найден id')
    }
  }
}

export default Graph