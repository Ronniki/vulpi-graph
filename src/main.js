import Graph from '@/vul-graph.js'
import css from '@/vul-graph.css'

const data = fetch('https://simple-graph-f775b.firebaseio.com/points.json', {
  headers: {
    'Content-type': 'application/json'
  }
})
.then(data => data.json())

data.then(data => {
  data.forEach((item, index) => {
    if (index == 0) {
      const options = {
        colorLine: 'rgba(0, 120, 0, 1)',
        data: item,
        id: `graph-${index + 1}`
      }
      const graph = new Graph(options)
      graph.init()
    } else if (index == 1){
      const options = {
        colorLine: 'rgba(0, 0, 100, 0.6)',
        data: item,
        textX: 'Любая подпись X',
        id: `graph-${index + 1}`
      }
      const graph = new Graph(options)
      graph.init()
    } else if (index == 2) {
      const options = {
        colorLine: 'rgba(20, 20, 80, 1)',
        data: item,
        textY: 'Ось Y',
        id: `graph-${index + 1}`
      }
      const graph = new Graph(options)
      graph.init()
    } else {
      const options = {
        colorLine: 'rgba(150, 0 , 14, 0.8)',
        data: item,
        id: `graph-${index + 1}`
      }
      const graph = new Graph(options)
      graph.init()
    }
  
  })
})